#include <allegro.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <allegro/font.h>
//#define SCREEN_W 640
//#define SCREEN_H 480
#include "head.h"

int *seq();

void mostra(int*);

int dif=0,somio;
volatile int ticks = 0;

SAMPLE *verde,*vermelho,*azul,*amarelo;
FILE *som;

typedef struct {
        char nome[128];
        int score;
} player;

inline void init(){
	allegro_init();
	set_color_depth(32);
	install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED,SCREEN_W,SCREEN_H, 0, 0);

	install_timer();
	install_keyboard();
	install_mouse();
}

void ticker(){
	ticks++;
}
END_OF_FUNCTION(ticker)
const int updates_per_second = 60;
inline void deinit(){
	clear_keybuf();
	allegro_exit();
}

char *getText(int score){

    LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));
	int k;
    char *edittext;
    edittext=(char*)malloc(128*sizeof(char));
    for(k=0;k<128;k++)edittext[k]=' ';
    BITMAP *fundo = create_bitmap(640,480);
    fundo=load_bitmap("imagens\\fim.bmp",NULL);
    BITMAP *buffer = create_bitmap(640,480);
    DATAFILE *rscfnt = NULL;
    rscfnt=load_datafile("segoe.dat");

    int caret=0;
    int l=0,corl=0,cor=1,cont=0,r,g,b;

    memset(edittext, 0, sizeof(edittext));
    key[KEY_ENTER]=NULL;



        while (l == 0)
        {
            if(cont>32) cont=0;
            while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)
		{
			int old_ticks = ticks;

            if(cont<8){r=32;g=192;b=66;}
                else if(cont<16){r=224;g=32;b=64;}
                        else if(cont<24){r=0;g=160;b=192;}
                                else if(cont<32){r=255;g=255;b=0;}

			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}
              fflush(stdin);
              clear(buffer);
              draw_sprite(buffer, fundo, 0, 0);
              if(cont==32) cor++;
              if(cor%2==0) corl=makecol(255, 255, 255);
                else corl=makecol(0, 0, 0);
              vline(buffer, ( caret * 20 ) + 270, 285, 315, corl);
              if(keypressed())
              {
                       int  newkey   = readkey();
                       char ASCII    = newkey & 0xff;
                       char scancode = newkey >> 8;

                       /* a character key was pressed; add it to the string */
                       if(ASCII >= 32 && ASCII <= 126)
                       {
                                                if(caret < 128 - 1)
                                                {
                                                         edittext[caret] = ASCII;
                                                         caret++;
                                                         edittext[caret] = ' ';
                                }
                       }
                       else if(scancode == KEY_BACKSPACE)
                       {
                            if (caret > 0) caret--;
                            edittext[caret] = ' ';
                       }
                       else if(key[KEY_ENTER])
                       {
                                            l = 1;
                                            caret++;
                                            edittext[caret]='\0';
                       }
                }
                textprintf_ex(buffer,(FONT *)rscfnt[1].dat,SCREEN_W / 2-170, SCREEN_H / 2-100,
                        makecol(r, g, b),-1, "VOCE ERROU");
                textprintf_ex(buffer,(FONT *)rscfnt[0].dat,SCREEN_W / 2-145, SCREEN_H / 2-20,
                        makecol(0, 0, 0),-1, "SUA PONTUACAO FOI DE %d PONTOS",score);
                textprintf_ex(buffer,(FONT *)rscfnt[0].dat,SCREEN_W / 2-128, SCREEN_H / 2,
                        makecol(0, 0, 0),-1, "DIGITE SEU NOME PARA SALVAR");

                cont++;
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat, 270, 280,makecol(0, 0, 0), -1, "%s", edittext);
                blit(buffer, screen, 0, 0, 0, 0, 640, 480);
         }


     /*destroy_bitmap(buffer);
     destroy_bitmap(fundo);*/
     destroy_bitmap(fundo);
     destroy_bitmap(buffer);
     return edittext;
}

void inverte(player rank[],int tam){

    player aux;
    int i;
    for(i=0;i<tam/2;i++){
        aux=rank[i];
        rank[i]=rank[tam-1-i];
        rank[tam-1-i]=aux;
    }
}

void ordena(player numbers[],int array_size){
  int i, j;
  player temp;

  for (i = (array_size - 1); i > 0; i--)
  {
    for (j = 1; j <= i; j++)
    {
      if (numbers[j-1].score > numbers[j].score)
      {
        temp = numbers[j-1];
        numbers[j-1] = numbers[j];
        numbers[j] = temp;
      }
    }
  }
}

void ranking(int pts,char *name){

    player *rank;
    FILE *fpo;
    int i;
    rank=(player*)malloc(11*sizeof(player));

    //for(i=0;i<11;i++) rank[i].score=0;

    fpo=fopen("ranking.txt","r+");
    fseek(fpo,(sizeof(player)*10*dif),0);
    fread(rank,sizeof(player),10,fpo);
    //for(i=0;i<10;i++) rank[i].score=0;
    //for(i=0;i<10;i++) strcpy(rank[i].nome,"-");
    if(pts>rank[9].score){
        strcpy(rank[9].nome,name);
        rank[9].score=pts;
    }
    ordena(rank,10);
    inverte(rank,10);
    //printf("\n\RANKING:\n");
    //for(i=0;i<10;i++)printf("nome:%s: %i\n\n",rank[i].nome,rank[i].score);
    fseek(fpo,(sizeof(player)*10*dif),0);
    fwrite(rank,sizeof(player),10,fpo);
    fclose(fpo);

}

void inst(char *string){
    LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));

    BITMAP *fundo,*buffer;
    DATAFILE *rscfnt = NULL;
    rscfnt = load_datafile("segoe.dat");

    int cont=0,r,g,b;
    fundo=create_bitmap(640,480);
    fundo=load_bitmap("imagens\\genius.bmp",NULL);
    buffer=create_bitmap(640,480);


	while(!key[KEY_ENTER])
	{
	    if(cont>32) cont=0;
		while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)		{
			int old_ticks = ticks;
            //DoLogic();
            if(cont<8){r=32;g=192;b=66;}
                else if(cont<16){r=224;g=32;b=64;}
                        else if(cont<24){r=0;g=160;b=192;}
                                else if(cont<32){r=255;g=255;b=0;}
			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}
        clear(buffer);
        draw_sprite(buffer,fundo,0,0);
        blit(buffer,screen, 0, 0, 0, 0, 640,480);
        textout_centre_ex(screen,(FONT *)rscfnt[0].dat, string,
                        SCREEN_W / 2, SCREEN_H / 2,
                        makecol(r, g, b), makecol(255, 255, 255));

        cont++;

    }
    destroy_bitmap(fundo);
    destroy_bitmap(buffer);
}

void sons(int cor){

    verde=load_sample("sons\\0.wav");
    vermelho=load_sample("sons\\1.wav");
    azul=load_sample("sons\\2.wav");
    amarelo=load_sample("sons\\3.wav");

    if(somio==0) return;
        else if(cor==0) play_sample(verde, 255, 127, 1000, 0);
                else if(cor==1) play_sample(vermelho, 255, 127, 1000, 0);
                    else if(cor==2) play_sample(azul, 255, 127, 1000, 0);
                        else if(cor==3 )play_sample(amarelo, 255, 127, 1000, 0);


}

void mostra(int*vet,int num){
    BITMAP *fundo,*buffer,*piscah,*piscav;
    int cont;
    fundo=create_bitmap(640,480);
    fundo=load_bitmap("imagens\\genius.bmp",NULL);
    buffer=create_bitmap(640,480);
    piscah=create_bitmap(490,150);
    clear_to_color(piscah,makecol(0,0,0));
    piscav=create_bitmap(150,340);
    clear_to_color(piscav,makecol(0,0,0));

    for (cont=0;cont<num;cont++){
        clear(buffer);
        draw_sprite(buffer,fundo,0,0);
        blit(buffer,screen, 0, 0, 0, 0, 640,480);
        rest(300);
        //textprintf_ex(buffer,font,300,220,makecol(0,0,0),-1,"vet[%d]", cont);
        if(vet[cont]==0){
            draw_sprite(buffer,piscah,0,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            sons(vet[cont]);
            rest(500);
        }
            else if(vet[cont]==1){
                    draw_sprite(buffer,piscav,490,0);
                    blit(buffer,screen, 0, 0, 0, 0, 640,480);
                    sons(vet[cont]);
                    rest(500);
                 }
                    else if(vet[cont]==2){
                            draw_sprite(buffer,piscav,0,150);
                            blit(buffer,screen, 0, 0, 0, 0, 640,480);
                            sons(vet[cont]);
                            rest(500);
                         }
                            else if(vet[cont]==3){
                                    draw_sprite(buffer,piscah,150,340);
                                    blit(buffer,screen, 0, 0, 0, 0, 640,480);
                                    sons(vet[cont]);
                                    rest(500);
                                 }

    }
    destroy_bitmap(fundo);
    destroy_bitmap(buffer);
    destroy_bitmap(piscah);
    destroy_bitmap(piscav);
}

void score(){

    LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));

	player *rankf,*rankm,*rankd;
    FILE *ff,*fm,*fd;
    int i;
    rankf=(player*)malloc(10*sizeof(player));
    rankm=(player*)malloc(10*sizeof(player));
    rankd=(player*)malloc(10*sizeof(player));

    //for(i=0;i<11;i++) rank[i].score=0;

    ff=fopen("ranking.txt","r+");
    fseek(ff,0,0);
    fread(rankf,sizeof(player),10,ff);
    fclose(ff);

    fm=fopen("ranking.txt","r+");
    fseek(fm,sizeof(player)*10,0);
    fread(rankm,sizeof(player),10,fm);
    fclose(fm);

    fd=fopen("ranking.txt","r+");
    fseek(fd,sizeof(player)*20,0);
    fread(rankd,sizeof(player),10,fd);
    fclose(fd);

    BITMAP *fundo,*buffer,*seta;
    DATAFILE *rscfnt = NULL;
    rscfnt=load_datafile("segoe.dat");
    fundo=create_bitmap(640,480);
    fundo=load_bitmap("imagens\\ranking.bmp",NULL);
    buffer=create_bitmap(640,480);
    seta=create_bitmap(190,25);
    seta=load_bitmap("imagens\\seta.bmp",NULL);
    draw_sprite(buffer,fundo,0,0);
    blit(buffer,screen, 0, 0, 0, 0, 640,480);


	while(!key[KEY_ESC]){
		while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)
		{
			int old_ticks = ticks;

            //DoLogic();

			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}
		if(mouse_x<190 && mouse_y<120 && mouse_b&1){
            clear(buffer);
            draw_sprite(buffer,fundo,0,0);
            draw_sprite(buffer,seta,0,0);
            for(i=0;i<10;i++){
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,250,50+40*i,makecol(0,0,0),-1,"%i",i+1);
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,350,50+40*i,makecol(0,0,0),-1,"%s",rankf[i].nome);
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,500,50+40*i,makecol(0,0,0),-1,"%i",rankf[i].score);
                blit(buffer,screen, 0, 0, 0, 0, 640,480);
                mouse_b=0;
            }
		}
		if(mouse_x<190 && mouse_y> 120 && mouse_y<240 && mouse_b&1){
            clear(buffer);
            draw_sprite(buffer,fundo,0,0);
            draw_sprite(buffer,seta,0,120);
            for(i=0;i<10;i++){
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,250,50+40*i,makecol(0,0,0),-1,"%i",i+1);
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,350,50+40*i,makecol(0,0,0),-1,"%s",rankm[i].nome);
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,500,50+40*i,makecol(0,0,0),-1,"%i",rankm[i].score);
                blit(buffer,screen, 0, 0, 0, 0, 640,480);
                mouse_b=0;
            }
		}
		if(mouse_x<190 && mouse_y>240 && mouse_y<360 && mouse_b&1){
		    clear(buffer);
            draw_sprite(buffer,fundo,0,0);
            draw_sprite(buffer,seta,0,240);
            for(i=0;i<10;i++){
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,250,50+40*i,makecol(0,0,0),-1,"%i",i+1);
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,350,50+40*i,makecol(0,0,0),-1,"%s",rankd[i].nome);
                textprintf_ex(buffer,(FONT*)rscfnt[2].dat,500,50+40*i,makecol(0,0,0),-1,"%i",rankd[i].score);
                blit(buffer,screen, 0, 0, 0, 0, 640,480);
                mouse_b=0;
            }
		}
		if(mouse_x<190 && mouse_y>360 && mouse_b&1) return;

        //draw_sprite(buffer,fundo,0,0);
        show_mouse(screen);
        //blit(buffer,screen, 0, 0, 0, 0, 640,480);
	}
}

void limpa(BITMAP *buffer,int d){

    LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));
    BITMAP *conf,*temp,*marca;
    conf=create_bitmap(213,120);
    temp=create_bitmap(640,480);
    conf=load_bitmap("imagens\\confere.bmp",NULL);
    marca=create_bitmap(84,49);
    marca=load_bitmap("imagens\\marca.bmp",NULL);
    draw_sprite(temp,buffer,0,0);
    player *rank;
    FILE *fpo;
    int x=2,i;
    rank=(player*)malloc(10*sizeof(player));

    bool quit = false;

	while(!key[KEY_ESC])
	{
		while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)
		{
			int old_ticks = ticks;

            //DoLogic();

			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}
        if(mouse_x>453 && mouse_x<536 && mouse_y<104  &&  mouse_y>56 && mouse_b&1) x=0;
        if(mouse_x>542 && mouse_x<625 && mouse_y<104  &&  mouse_y>56 && mouse_b&1) x=1;
        draw_sprite(temp,conf,430,0);
        blit(temp,screen, 0, 0, 0, 0, 640,480);
        if(!x){
            draw_sprite(temp,marca,453,55);
            blit(temp,screen, 0, 0, 0, 0, 640,480);rest(500);
            fpo=fopen("ranking.txt","r+");
            fseek(fpo,(sizeof(player)*10*d),0);
            fread(rank,sizeof(player),10,fpo);
            for(i=0;i<10;i++) rank[i].score=0;
            for(i=0;i<10;i++) strcpy(rank[i].nome,"-");
            fseek(fpo,(sizeof(player)*10*d),0);
            fwrite(rank,sizeof(player),10,fpo);
            fclose(fpo);
            destroy_bitmap(temp);
            destroy_bitmap(conf);
            destroy_bitmap(marca);
            return;
        }

        if(x==1){
            destroy_bitmap(temp);
            destroy_bitmap(conf);
            destroy_bitmap(marca);
            return;
        }
        show_mouse(screen);
    }


}

void settings(){

    LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));

	DATAFILE *rscfnt = NULL;
    rscfnt = load_datafile("segoe.dat");

    int a=0,b=0,c=0;

    BITMAP *fundo,*buffer,*seta,*sett3,*sett2,*temp,*selec;

    fundo=create_bitmap(640,480);
    buffer=create_bitmap(640,480);
    seta=create_bitmap(213,25);
    sett3=create_bitmap(640,480);
    sett2=create_bitmap(640,480);
    selec=create_bitmap(213,120);
    temp=create_bitmap(640,480);
    fundo=load_bitmap("imagens\\settings.bmp",NULL);
    seta=load_bitmap("imagens\\seta2.bmp",NULL);
    sett2=load_bitmap("imagens\\settings2.bmp",NULL);
    sett3=load_bitmap("imagens\\settings3.bmp",NULL);
    selec=load_bitmap("imagens\\selec.bmp",NULL);
    draw_sprite(buffer,fundo,0,0);
    blit(buffer,screen, 0, 0, 0, 0, 640,480);

	bool quit = false;

	while(!key[KEY_ESC])
	{
		while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)
		{
			int old_ticks = ticks;

            //DoLogic();

			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}
        if(mouse_x<190 && mouse_y<120 && mouse_b&1){
            clear(buffer);
            draw_sprite(buffer,fundo,0,0);
            draw_sprite(buffer,sett2,0,0);
            draw_sprite(buffer,seta,0,3);
            draw_sprite(temp,buffer,0,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            mouse_b=0;
            a=1;b=0;c=0;
        }
        if(mouse_x>216 && mouse_x<427 && mouse_y<120 && mouse_b&1 && a==1){
            draw_sprite(buffer,selec,216,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
            dif=0;
        }
        if(dif==0 && a==1){
            draw_sprite(buffer,selec,216,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x>216 && mouse_x<427 && mouse_y<240 && mouse_y>120 && mouse_b&1 && a==1){
            draw_sprite(buffer,selec,216,120);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
            dif=1;
        }
        if( dif==1 && a==1){
            draw_sprite(buffer,selec,216,120);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x>216 && mouse_x<427 && mouse_y<360 && mouse_y>240 && mouse_b&1 && a==1){
            draw_sprite(buffer,selec,216,240);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
            dif=2;
        }
        if(dif==2 && a==1){
            draw_sprite(buffer,selec,216,240);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x<213 && mouse_y> 120 && mouse_y<240 && mouse_b&1){
            clear(buffer);
            draw_sprite(buffer,fundo,0,0);
            draw_sprite(buffer,sett3,0,0);
            draw_sprite(buffer,seta,0,123);
            draw_sprite(temp,buffer,0,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            mouse_b=0;
            a=0;b=1;c=0;
		}
        if(mouse_x>216 && mouse_x<427 && mouse_y<120 && mouse_b&1 && b==1){
                draw_sprite(buffer,selec,216,0);
                blit(buffer,screen, 0, 0, 0, 0, 640,480);
                draw_sprite(buffer,temp,0,0);
                somio=1;
                som=fopen("sons\\som.bin","w");
                fprintf(som,"%d",somio);
                fclose(som);
        }
        if(somio==1 && b==1){
            draw_sprite(buffer,selec,216,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x>216 && mouse_x<427 && mouse_y<240 && mouse_y>120 && mouse_b&1 && b==1){
                draw_sprite(buffer,selec,216,120);
                blit(buffer,screen, 0, 0, 0, 0, 640,480);
                draw_sprite(buffer,temp,0,0);
                somio=0;
                som=fopen("sons\\som.bin","w");
                fprintf(som,"%d",somio);
                fclose(som);
        }
        if(somio==0 && b==1){
            draw_sprite(buffer,selec,216,120);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x<213 && mouse_y>240 && mouse_y<360 && mouse_b&1){
		    clear(buffer);
		    draw_sprite(buffer,fundo,0,0);
            draw_sprite(buffer,sett2,0,0);
            draw_sprite(buffer,seta,0,243);
            draw_sprite(temp,buffer,0,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            mouse_b=0;
            a=0;b=0;c=1;
		}
        if(mouse_x>216 && mouse_x<427 && mouse_y<120 && mouse_b&1 && c==1){
            draw_sprite(buffer,seta,215,3);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            limpa(buffer,0);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x>216 && mouse_x<427 && mouse_y<240 && mouse_y>120 && mouse_b&1 && c==1){
            draw_sprite(buffer,seta,215,123);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            limpa(buffer,1);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x>216 && mouse_x<427 && mouse_y<360 && mouse_y>240 && mouse_b&1 && c==1){
            draw_sprite(buffer,seta,215,243);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            limpa(buffer,2);
            blit(buffer,screen, 0, 0, 0, 0, 640,480);
            draw_sprite(buffer,temp,0,0);
        }
        if(mouse_x<213 && mouse_y>360 && mouse_b&1) return;

        show_mouse(screen);
	}
	destroy_bitmap(fundo);
	destroy_bitmap(buffer);
	destroy_bitmap(seta);
	destroy_bitmap(sett2);
	destroy_bitmap(sett3);
	destroy_bitmap(selec);
	destroy_bitmap(temp);

}

int jogo(){

	LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));

	DATAFILE *rscfnt = NULL;
    rscfnt = load_datafile("segoe.dat");

    BITMAP *fundo,*buffer,*piscah,*piscav;
    int *vet,cor,i=0,cont,rst=0,num=3,score=0;
    char ini[50]="APERTE ENTER PARA COMECAR",*nome;
    /*FILE *fpi;
    fpi=fopen("score.txt");*/

    fundo=create_bitmap(640,480);
    buffer=create_bitmap(640,480);
    fundo=load_bitmap("imagens\\genius.bmp",NULL);
    piscah=create_bitmap(490,150);
    clear_to_color(piscah,makecol(0,0,0));
    piscav=create_bitmap(150,340);
    clear_to_color(piscav,makecol(0,0,0));
    vet=seq();
    inst(ini);
    mostra(vet,num);

	bool quit = false;

	while(!key[KEY_ESC])
	{
		while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)
		{
			int old_ticks = ticks;

            //DoLogic();

			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}
		rst=0;
        clear(buffer);
        draw_sprite(buffer,fundo,0,0);
        //buffer=load_bitmap("genius.bmp",NULL);
        //for (cont=0;cont<5;cont++) textprintf_ex(buffer,font,300,240+cont*10,makecol(0,0,0),-1,"%d ", vet[cont]);

        if(mouse_x<490 && mouse_y<150 && mouse_b&1) {
            draw_sprite(buffer,piscah,0,0);
            cor=0;
            rst=1;
            if(cor!=vet[i]){
                nome=getText(score);
                ranking(score,nome);
                destroy_bitmap(fundo);
                destroy_bitmap(buffer);
                destroy_bitmap(piscah);
                destroy_bitmap(piscav);
                return 0;
            }
            i++;
            if(i==num || (dif==1 && i==num-1) || num==3 || dif==2)score+=100;
                else score+=10;

            mouse_b=0;
        }
        if(mouse_x>490 && mouse_y<339 && mouse_b&1){
            draw_sprite(buffer,piscav, 490, 0);
            cor=1;
            rst=1;
            if(cor!=vet[i]){
                nome=getText(score);
                ranking(score,nome);
                destroy_bitmap(fundo);
                destroy_bitmap(buffer);
                destroy_bitmap(piscah);
                destroy_bitmap(piscav);
                return 0;
            }
            i++;
            if(i==num || (dif==1 && i==num-1) || num==3 || dif==2)score+=100;
                else score+=10;
            mouse_b=0;
        }
        if(mouse_x<150 && mouse_y>150 && mouse_b&1){
            draw_sprite(buffer,piscav,0,150);
            cor=2;
            rst=1;
            if(cor!=vet[i]){
                nome=getText(score);
                ranking(score,nome);
                destroy_bitmap(fundo);
                destroy_bitmap(buffer);
                destroy_bitmap(piscah);
                destroy_bitmap(piscav);
                return 0;
            }
            i++;
            if(i==num || (dif==1 && i==num-1) || num==3 || dif==2)score+=100;
                else score+=10;
            mouse_b=0;
        }
        if(mouse_x>150 && mouse_y>339 && mouse_b&1){
            draw_sprite(buffer,piscah,150,340);
            cor=3;
            rst=1;
            if(cor!=vet[i]){
                nome=getText(score);
                ranking(score,nome);
                destroy_bitmap(fundo);
                destroy_bitmap(buffer);
                destroy_bitmap(piscah);
                destroy_bitmap(piscav);
                return 0;
            }
            i++;
            if(i==num || (dif==1 && i==num-1) || num==3 || dif==2)score+=100;
                else score+=10;;
            mouse_b=0;
        }
        select_mouse_cursor(1);
        textprintf_ex(buffer,(FONT*)rscfnt[1].dat,270,220,makecol(0,0,0),-1,"%d",score);
        show_mouse(screen);
        blit(buffer,screen, 0, 0, 0, 0, 640,480);
        if(rst){
            sons(cor);
            rest(200);
        }
        if(i==num){
            if(dif==0 || dif==2) num++;
                else if(dif==1) num+=2;

            if(dif==2) if(dif==2) vet=seq();
            mostra(vet,num);
            i=0;
            //printf("\n\n SCORE: %i ",score);
        }
	}
	destroy_bitmap(fundo);
	destroy_bitmap(buffer);
	destroy_bitmap(piscah);
	destroy_bitmap(piscav);
	return 0;
}

int menu(){

    LOCK_VARIABLE(ticks);
	LOCK_FUNCTION(ticker);
	install_int_ex(ticker, BPS_TO_TIMER(updates_per_second));
    BITMAP *fundo,*buffer,*play,*set,*rank;
    int rst,D;
    fundo=create_bitmap(640,480);
    fundo=load_bitmap("imagens\\menu.bmp",NULL);
    buffer=create_bitmap(640,480);
    play=create_bitmap(100,100);
    play=load_bitmap("imagens\\playb.bmp",NULL);
    set=create_bitmap(100,100);
    set=load_bitmap("imagens\\setb.bmp",NULL);
    rank=create_bitmap(100,100);
    rank=load_bitmap("imagens\\rankb.bmp",NULL);
    som=fopen("sons\\som.bin","r");
    fscanf(som,"%d",&somio);
    fclose(som);


    while(!key[KEY_ESC]){

        while(ticks == 0)
		{
			rest(1);//rest until a full tick has passed
		}

		while(ticks > 0)
		{
			int old_ticks = ticks;

            //DoLogic();

			ticks--;
			if(old_ticks <= ticks)//logic is taking too long: abort and draw a frame
				break;
		}


        rst=0;
        clear(buffer);
        draw_sprite(buffer,fundo,0,0);

        if(mouse_x>160 && mouse_x<250 && mouse_y>200 && mouse_y<300){
            D=sqrt(((abs(205-mouse_x))^2)+((abs(248-mouse_y)^2)));
            if(D<45){
                draw_sprite(buffer,play,155,199);
                rst=1;
                if(mouse_b&1) return 1;
            }
        }
        if(mouse_x>275 && mouse_x<375 && mouse_y>200 && mouse_y<300){
            draw_sprite(buffer,set,274,201);
            rst=1;
            if(mouse_b&1) return 2;
        }
        if(mouse_x>386 && mouse_x<486 && mouse_y>190 && mouse_y<300){
            draw_sprite(buffer,rank,387,203);
            rst=1;
            if(mouse_b&1) return 3;
        }
        show_mouse(screen);
        blit(buffer,screen, 0, 0, 0, 0, 640,480);
        if(rst) rest(100);
    }
    return 0;

}

int main(){

    init();
    int ret;
    here:
    ret=menu();
    switch (ret){
        case 0:
            return(EXIT_SUCCESS);
            break;
        case 1:
            jogo();
            goto here;
            break;
        case 2:
            settings();
            goto here;
            break;
        case 3:
            score();
            goto here;
            break;
        default:
            return(EXIT_FAILURE);
    }
  return 0;
}
END_OF_MAIN()
